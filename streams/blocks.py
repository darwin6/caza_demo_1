from wagtail.core import blocks
from wagtail.images.blocks import ImageChooserBlock
from django.db import models


class Block_about(blocks.StructBlock):
    label = blocks.CharBlock(required=True, help_text="Add your label here")
    title = blocks.CharBlock(required=True, help_text="Add your title here")
    subtitle = blocks.RichTextBlock(
        required=True, help_text="Add your subtitle here")
    chef_name = blocks.CharBlock(
        required=True, help_text="Add your Chef name here")
    chef_position = blocks.CharBlock(
        required=True, help_text="Add your Chef position here")
    image_chef = ImageChooserBlock(required=True)
    image_1 = ImageChooserBlock(required=True)
    image_2 = ImageChooserBlock(required=True)
    image_3 = ImageChooserBlock(required=True)
    image_4 = ImageChooserBlock(required=True)

    class Meta:
        template = "streams/block_section_about.html"
        icon = "edit"
        label = "section about"


class Block_reservation(blocks.StructBlock):
    title = blocks.CharBlock(required=True, help_text="Add your title here")
    subtitle = blocks.RichTextBlock(
        required=True, help_text="Add your subtitle here")
    button_text = blocks.CharBlock(
        required=True, help_text="Add your button text here")
    image_side = ImageChooserBlock(required=True)

    class Meta:
        template = "streams/block_section_reservation.html"
        icon = "edit"
        label = "section reservation"


class Block_menu(blocks.StructBlock):
    label = blocks.CharBlock(required=True, help_text="Add your label here")
    title = blocks.CharBlock(required=True, help_text="Add your title here")
    subtitle = blocks.RichTextBlock(
        required=True, help_text="Add your subtitle here")
    menu_link_text = blocks.CharBlock(
        required=True, help_text="Add your menu link text here")
    menu_title_1 = blocks.CharBlock(
        required=True, help_text="Add your menu title 1 here")
    menu_subtitle_1 = blocks.TextBlock(
        required=True, help_text="Add your menu subtitle 1 here")
    menu_image_1 = ImageChooserBlock(required=True)
    menu_title_2 = blocks.CharBlock(
        required=True, help_text="Add your menu title 2 here")
    menu_subtitle_2 = blocks.TextBlock(
        required=True, help_text="Add your menu subtitle 2 here")
    menu_image_2 = ImageChooserBlock(required=True)
    menu_title_3 = blocks.CharBlock(
        required=True, help_text="Add your menu title 3 here")
    menu_subtitle_3 = blocks.TextBlock(
        required=True, help_text="Add your menu subtitle 3 here")
    menu_image_3 = ImageChooserBlock(required=True)
    menu_title_4 = blocks.CharBlock(
        required=True, help_text="Add your menu title 4 here")
    menu_subtitle_4 = blocks.TextBlock(
        required=True, help_text="Add your menu subtitle 4 here")
    menu_image_4 = ImageChooserBlock(required=True)

    class Meta:
        template = "streams/block_section_menu.html"
        icon = "edit"
        label = "section menu"


class Block_review(blocks.StructBlock):
    bg_image = ImageChooserBlock(required=True)
    cards = blocks.ListBlock(
        blocks.StructBlock(
            [

                ('card_title', blocks.CharBlock(
                    required=True, help_text='Add your title here')),
                ('card_subtitle', blocks.TextBlock(
                    required=True, help_text='Add your subtitle here')),
                ('star', blocks.IntegerBlock(required=True, min_value=1,
                                             max_value=5, help_text='Add review star minimum 1 and maximum 5')),
            ]
        )
    )

    class Meta:
        template = "streams/block_section_review.html"
        icon = "edit"
        label = "section review"


class Block_gallery(blocks.StructBlock):
    gallery_image_1 = ImageChooserBlock(required=True)
    gallery_image_2 = ImageChooserBlock(required=True)
    gallery_image_3 = ImageChooserBlock(required=True)
    gallery_image_4 = ImageChooserBlock(required=True)
    gallery_image_5 = ImageChooserBlock(required=True)
    gallery_image_6 = ImageChooserBlock(required=True)
    gallery_image_7 = ImageChooserBlock(required=True)
    gallery_image_8 = ImageChooserBlock(required=True)

    class Meta:
        template = "streams/block_section_gallery.html"
        icon = "edit"
        label = "section gallery"


class Block_service(blocks.StructBlock):
    label = blocks.CharBlock(required=True, help_text="Add your label here")
    title = blocks.CharBlock(required=True, help_text="Add your title here")
    subtitle = blocks.RichTextBlock(
        required=True, help_text="Add your subtitle here")
    ps_text = blocks.RichTextBlock(
        required=False, help_text="Add your ps. text  here")
    service_image_1 = ImageChooserBlock(required=True)
    service_title_1 = blocks.CharBlock(
        required=True, help_text="Add your service title 1 here")
    service_subtitle_1 = blocks.TextBlock(
        required=True, help_text="Add your service subtitle 1 here")
    service_image_2 = ImageChooserBlock(required=True)
    service_title_2 = blocks.CharBlock(
        required=True, help_text="Add your service title 2 here")
    service_subtitle_2 = blocks.TextBlock(
        required=True, help_text="Add your service subtitle 2 here")
    service_image_3 = ImageChooserBlock(required=True)
    service_title_3 = blocks.CharBlock(
        required=True, help_text="Add your service title 3 here")
    service_subtitle_3 = blocks.TextBlock(
        required=True, help_text="Add your service subtitle 3 here")
    service_image_4 = ImageChooserBlock(required=True)
    service_title_4 = blocks.CharBlock(
        required=True, help_text="Add your menu service 4 here")
    service_subtitle_4 = blocks.TextBlock(
        required=True, help_text="Add your service subtitle 4 here")

    class Meta:
        template = "streams/block_section_service.html"
        icon = "edit"
        label = "section service"


class Block_menu_details(blocks.StructBlock):
    categories_title = blocks.CharBlock(
        required=True, help_text="Add your categories title here")
    categories_image = ImageChooserBlock(required=True)
    menu_lists = blocks.ListBlock(
        blocks.StructBlock(
            [

                ('list_title', blocks.CharBlock(
                    required=True, help_text='Add your menu title here')),
                ('list_subtitle', blocks.RichTextBlock(
                    required=True, help_text='Add your menu subtitle here')),
                ('list_price', blocks.DecimalBlock(
                    required=True, help_text='Add your menu price here', max_digits=5, decimal_places=2)),
                ('list_image', ImageChooserBlock(required=True)),

            ]
        )
    )

    class Meta:
        template = "streams/block_section_menu_details.html"
        icon = "edit"
        label = "section menu"
