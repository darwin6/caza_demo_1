# Generated by Django 2.2.1 on 2019-05-26 18:59

from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    initial = True

    dependencies = [
        ('wagtailimages', '0001_squashed_0021'),
        ('wagtailcore', '0041_group_collection_permissions_verbose_name_plural'),
    ]

    operations = [
        migrations.CreateModel(
            name='SocialMediaSettings',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('facebook', models.URLField(blank=True, help_text='Facebook URL', null=True)),
                ('twitter', models.URLField(blank=True, help_text='Twitter URL', null=True)),
                ('youtube', models.URLField(blank=True, help_text='Youtube Channel URL', null=True)),
                ('instagram', models.URLField(blank=True, help_text='Instagram URL', null=True)),
                ('site', models.OneToOneField(editable=False, on_delete=django.db.models.deletion.CASCADE, to='wagtailcore.Site')),
            ],
            options={
                'abstract': False,
            },
        ),
        migrations.CreateModel(
            name='BrandingSettings',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('footer_text', models.CharField(blank=True, help_text='Footer Text', max_length=100, null=True)),
                ('phone', models.CharField(blank=True, help_text='Add phone number here', max_length=100, null=True)),
                ('email', models.CharField(blank=True, help_text='Add Email here', max_length=100, null=True)),
                ('address_street', models.CharField(blank=True, help_text='Add your street here', max_length=100, null=True)),
                ('address_city', models.CharField(blank=True, help_text='Add your city here', max_length=100, null=True)),
                ('address_state', models.CharField(blank=True, help_text='Add your state here (Abbreviated)', max_length=100, null=True)),
                ('zip_code', models.CharField(blank=True, help_text='Add your zip code here', max_length=100, null=True)),
                ('hour_monday', models.CharField(blank=True, help_text='Add your hour here (Format eg. 10:00-22.00)', max_length=100, null=True)),
                ('hour_tuesday', models.CharField(blank=True, help_text='Add your hour here (Format eg. 10:00-22.00)', max_length=100, null=True)),
                ('hour_wednesday', models.CharField(blank=True, help_text='Add your hour here (Format eg. 10:00-22.00)', max_length=100, null=True)),
                ('hour_thursday', models.CharField(blank=True, help_text='Add your hour here (Format eg. 10:00-22.00)', max_length=100, null=True)),
                ('hour_friday', models.CharField(blank=True, help_text='Add your hour here (Format eg. 10:00-22.00)', max_length=100, null=True)),
                ('hour_saturday', models.CharField(blank=True, help_text='Add your hour here (Format eg. 10:00-22.00)', max_length=100, null=True)),
                ('hour_sunday', models.CharField(blank=True, help_text='Add your hour here (Format eg. 10:00-22.00)', max_length=100, null=True)),
                ('logo_image_color', models.ForeignKey(null=True, on_delete=django.db.models.deletion.SET_NULL, related_name='+', to='wagtailimages.Image')),
                ('logo_image_white', models.ForeignKey(null=True, on_delete=django.db.models.deletion.SET_NULL, related_name='+', to='wagtailimages.Image')),
                ('site', models.OneToOneField(editable=False, on_delete=django.db.models.deletion.CASCADE, to='wagtailcore.Site')),
            ],
            options={
                'abstract': False,
            },
        ),
    ]
