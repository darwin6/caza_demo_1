from django.db import models

from wagtail.admin.edit_handlers import FieldPanel, MultiFieldPanel
from wagtail.contrib.settings.models import BaseSetting, register_setting
from wagtail.images.edit_handlers import ImageChooserPanel


@register_setting
class SocialMediaSettings(BaseSetting):
    facebook = models.URLField(blank=True, null=True, help_text="Facebook URL")
    twitter = models.URLField(blank=True, null=True, help_text="Twitter URL")
    youtube = models.URLField(blank=True, null=True,
                              help_text="Youtube Channel URL")
    instagram = models.URLField(
        blank=True, null=True, help_text="Instagram URL")

    panels = [
        MultiFieldPanel([
            FieldPanel("facebook"),
            FieldPanel("twitter"),
            FieldPanel("youtube"),
            FieldPanel("instagram"),
        ], heading="Social Media Settings")
    ]


@register_setting
class BrandingSettings(BaseSetting):
    logo_image_color = models.ForeignKey(
        "wagtailimages.Image",
        null=True,
        blank=False,
        on_delete=models.SET_NULL,
        related_name="+"
    )
    logo_image_white = models.ForeignKey(
        "wagtailimages.Image",
        null=True,
        blank=False,
        on_delete=models.SET_NULL,
        related_name="+"
    )
    footer_text = models.CharField(
        blank=True, null=True, help_text="Footer Text", max_length=300)
    phone = models.CharField(blank=True, null=True,
                             help_text="Add phone number here", max_length=300)
    email = models.CharField(blank=True, null=True,
                             help_text="Add Email here", max_length=300)
    address = models.CharField(
        blank=True, null=True, help_text="Add your address here", max_length=300)
    license_reserve = models.CharField(
        blank=True, null=True, help_text="Add your license here", max_length=300)

    hour_monday = models.CharField(
        blank=True, null=True, help_text="Add your hour here (Format eg. 10:00-22.00)", max_length=100)
    hour_tuesday = models.CharField(
        blank=True, null=True, help_text="Add your hour here (Format eg. 10:00-22.00)", max_length=100)
    hour_wednesday = models.CharField(
        blank=True, null=True, help_text="Add your hour here (Format eg. 10:00-22.00)", max_length=100)
    hour_thursday = models.CharField(
        blank=True, null=True, help_text="Add your hour here (Format eg. 10:00-22.00)", max_length=100)
    hour_friday = models.CharField(
        blank=True, null=True, help_text="Add your hour here (Format eg. 10:00-22.00)", max_length=100)
    hour_saturday = models.CharField(
        blank=True, null=True, help_text="Add your hour here (Format eg. 10:00-22.00)", max_length=100)
    hour_sunday = models.CharField(
        blank=True, null=True, help_text="Add your hour here (Format eg. 10:00-22.00)", max_length=100)
    panels = [
        MultiFieldPanel([
            ImageChooserPanel("logo_image_color"),
            ImageChooserPanel("logo_image_white"),


        ], heading="Branding Settings"),
        MultiFieldPanel([
            FieldPanel("footer_text"),
            FieldPanel("phone"),
            FieldPanel("email"),
            FieldPanel("address"),
            FieldPanel("license_reserve"),




        ], heading="Footer Settings"),
        MultiFieldPanel([
            FieldPanel("hour_monday"),
            FieldPanel("hour_tuesday"),
            FieldPanel("hour_wednesday"),
            FieldPanel("hour_thursday"),
            FieldPanel("hour_friday"),
            FieldPanel("hour_saturday"),
            FieldPanel("hour_sunday"),


        ], heading="Hour Settings"),
    ]
