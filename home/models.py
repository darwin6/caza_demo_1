from django.db import models

from wagtail.core.models import Page, Orderable
from wagtail.core.fields import RichTextField
from wagtail.images.edit_handlers import ImageChooserPanel
from wagtail.admin.edit_handlers import FieldPanel, StreamFieldPanel, PageChooserPanel, InlinePanel, MultiFieldPanel
from modelcluster.fields import ParentalKey

from wagtail.core.fields import StreamField
from streams import blocks


class HomePageCarouselImage(Orderable):
    page = ParentalKey("home.Homepage", related_name="carousel_images")
    carousel_image = models.ForeignKey(
        "wagtailimages.Image",
        null=True,
        blank=False,
        on_delete=models.SET_NULL,
        related_name="+"
    )

    panels = [
        ImageChooserPanel("carousel_image")
    ]


class HomePage(Page):
    template = "home/home_page.html"
    max_count = 1
    header_title = models.CharField(max_length=100, blank=False, null=True)
    header_subtitle = models.CharField(max_length=100, blank=False, null=True)

    content = StreamField(
        [
            ("block_about", blocks.Block_about()),
            ("block_reservation", blocks.Block_reservation()),
            ("block_menu", blocks.Block_menu()),
            ("block_review", blocks.Block_review()),
            ("block_gallery", blocks.Block_gallery()),
            ("block_service", blocks.Block_service()),

        ],
        null=True,
        blank=True
    )

    content_panels = Page.content_panels + [
        MultiFieldPanel([
            FieldPanel("header_title"),
            FieldPanel("header_subtitle"),


        ], heading="Header Options"),
        MultiFieldPanel([
            InlinePanel("carousel_images", max_num=5,
                        min_num=1, label="Image"),
        ], heading="Carousel Images"),
        StreamFieldPanel("content"),
    ]

    class Meta:
        verbose_name = "Home Page"
        verbose_name_plural = "Home Pages"
