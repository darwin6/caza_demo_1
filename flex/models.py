from django.db import models

from wagtail.admin.edit_handlers import FieldPanel, StreamFieldPanel
from wagtail.core.models import Page, Orderable
from wagtail.core.fields import StreamField
from modelcluster.fields import ParentalKey
from wagtail.admin.edit_handlers import FieldPanel, StreamFieldPanel, InlinePanel, MultiFieldPanel

from streams import blocks


class FlexPage(Page):
    template = "flex/flex_page.html"
    max_count = 1
    content = StreamField(
        [
            ("block_menu", blocks.Block_menu_details()),

        ],
        null=True,
        blank=True
    )

    content_panels = Page.content_panels + [

        StreamFieldPanel("content"),
    ]

    class Meta:
        verbose_name = "Flex Page"
        verbose_name_plural = "Flex Pages"
